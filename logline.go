package spectrum

import (
	"fmt"
	"strings"
	"time"

	"github.com/fatih/color"
)

var seperator = " "

type LogLine struct {
	Level   string    `json:"level"`
	Time    time.Time `json:"time"`
	Module  string    `json:"module"`
	Message string    `json:"message"`
	Err     error     `json:"error"`
	Misc    map[string]string
}

func (l LogLine) Print() {
	l.printLevel()
	l.printTime()
	l.printModule()
	l.printMessage()
	fmt.Printf("\n")
}

func (l LogLine) printLevel() {
	c := color.New()
	level := strings.ToUpper(l.Level)
	switch level {
	case "DEBUG":
		c = color.New(color.FgWhite)
	case "INFO":
		c = color.New(color.FgGreen)
	case "ERROR":
		c = color.New(color.FgRed)
	default:
		c = color.New(color.FgCyan)
	}
	c.Printf(level + seperator)
}

func (l LogLine) printString(n, s string) {
	s = strings.TrimSpace(s)
	if len(s) > 0 {
		if len(n) > 0 {
			fmt.Printf("%s: %s%s", n, s, seperator)
		} else {
			fmt.Printf("%s%s", s, seperator)
		}
	}
}

func (l LogLine) printTime() {
	if !l.Time.IsZero() {
		l.printString("", l.Time.String())
	}
}

func (l LogLine) printMessage() {
	l.printString("MSG", l.Message)
}

func (l LogLine) printModule() {
	l.printString("MODULE", l.Module)
}

func (l LogLine) printMisc() {
	for k, v := range l.Misc {
		l.printString(k, v)
	}
}
